
package MisAplicaciones;
import MisClases.Asignatura;
import MisClases.Tema;
import MisUtilerias.lecturaSC;

//Definir clase
public class PruebaAsignatura{
    private static Asignatura[] asignaturas;

    public static void main (String []ars){
        int OPC;

        do {
             mostrarListaOpciones();
             OPC = lecturaSC.leerEntero("Ingrese la opción a realizar", 1, 9);

              switch (OPC){
                  case 1:
                    crearAsignatura();
                  break;
                  case 2:
                      crearTema();
                  break;
                  case 3:
                      crearTodosLosTemas();
                  break;
                  case 4:
                    mostrarAsignatura();
                  break;
                  case 5:
                      mostrarTema();
                  break;
                  case 6:
                      mostrarTemas();
                  break;
                  case 7:
                      mostrarAsignaturas();
                  break;
                  case 8:
                      asignarCalif();
                  break;
                  case 9:
                      System.out.println("Saliendo del programa...");
                  break;

              }//Fin del switch

        }while(OPC != 9);


    }//Fin del main



        public static void mostrarListaOpciones(){
      String cadena =
                     "1._ Crear una asignatura " + " \n "
                  + "2._ Crear un tema "+ " \n "
                  + "3._ Crear todos los temas "+ " \n "
                  + "4._ Mostrar un asignatura "+ " \n "
                  + "5._ Mostrar un tema"+ " \n "
                  + "6._ Mostrar todos los temas"+ " \n "
                  + "7._ Mostrar todas las asignaturas"+ " \n "
                  + "8._ Asignar calificación"+ " \n "
                  + "9._ Salir ";
        System.out.println(cadena);
    }//Fin de MostrarLista


 public static void crearAsignatura() {
     int numAsignaturas = lecturaSC.leerEntero("Ingrese el numero  de asignaturas", 0);
     asignaturas = new Asignatura[numAsignaturas];

     for (int x = 0; x < asignaturas.length; x++) {
         int count = x + 1;
         System.out.println("Introduce los valores de la asignatura " + count);
         String nombre = lecturaSC.leerString("Ingrese el nombre de la asignatura");
         String clave = lecturaSC.leerString("Ingrese la clave de la asignatura");
         int credito = lecturaSC.leerEntero("Ingrese los creditos de la asignatura", 0);
         int numTemas = lecturaSC.leerEntero("Ingrese el numero de temas de la asignatura", 0);
         Asignatura asignatura = new Asignatura();
         asignatura.setNombre(nombre);
         asignatura.setClave(clave);
         asignatura.setCreditos(credito);
         asignatura.setNumTemas(numTemas);

         asignaturas[x] = asignatura;
     }
 }

     public static void crearTema() {
         if (asignaturas != null && asignaturas.length != 0) {
             System.out.println("No hay asignaturas creadas");
             return ;
         }

         for (int x = 0; x < asignaturas.length; x++) {
             System.out.println(x++ + ". " + asignaturas[x].getNombre());
         }

         int numAsignatura = lecturaSC.leerEntero("Número de asignatura a mostrar", 0, asignaturas.length);

         Asignatura asignatura = asignaturas[numAsignatura - 1];
         Tema[] temas = new Tema[asignatura.getNumTemas() - 1];

         for (int x = 0; x < asignatura.getNumTemas(); x++) {
             System.out.println("Introduce los valores del tema " + x++);
             String nombreTema = lecturaSC.leerString("Ingrese el nombre del tema");
             String competencia = lecturaSC.leerString("Ingrese la competencia del tema");
             int calif = lecturaSC.leerEntero("Ingrese la calficación del tema", -1, 101);
             Tema tema = new Tema();
             tema.setNombre(nombreTema);
             tema.setCompetencia(competencia);
             tema.setCalif(calif);
             temas[x] = tema;
         }

         asignatura.setTemas(temas);
     }

    public static void crearTodosLosTemas() {
        if (asignaturas != null && asignaturas.length != 0) {
            System.out.println("No hay asignaturas creadas");
            return ;
        }

        for (int x = 0; x < asignaturas.length; x++) {
            Asignatura asignatura = asignaturas[x];
            Tema[] temas = new Tema[asignatura.getNumTemas() - 1];

            for (int y = 0; y < asignatura.getNumTemas(); y++) {
                System.out.println("Introduce los valores del tema " + x++);
                String nombreTema = lecturaSC.leerString("Ingrese el nombre del tema");
                String competencia = lecturaSC.leerString("Ingrese la competencia del tema");
                int calif = lecturaSC.leerEntero("Ingrese la calficación del tema", -1, 101);
                Tema tema = new Tema();
                tema.setNombre(nombreTema);
                tema.setCompetencia(competencia);
                tema.setCalif(calif);
                temas[x] = tema;
            }
        }
    }

    public static void mostrarAsignatura() {
        if (asignaturas != null && asignaturas.length != 0) {
            System.out.println("No hay asignaturas creadas");
            return ;
        }

        for (int x = 0; x < asignaturas.length; x++) {
            System.out.println(x++ + ". " + asignaturas[x].getNombre());
        }

        int numAsignatura = lecturaSC.leerEntero("Número de asignatura a mostrar", 0, asignaturas.length);

        System.out.println(asignaturas[numAsignatura - 1].toString());
    }

     public static void mostrarTema(){
         if (asignaturas != null && asignaturas.length != 0) {
             System.out.println("No hay asignaturas creadas");
             return ;
         }

         for (int x = 0; x < asignaturas.length; x++) {
             System.out.println(x++ + ". " + asignaturas[x].getNombre());
         }

         int numAsignatura = lecturaSC.leerEntero("Número de asignatura a mostrar", 0, asignaturas.length);

         Asignatura asignatura = asignaturas[numAsignatura - 1];

         if (asignatura.getTemas() != null && asignatura.getTemas().length != 0) {
             System.out.println("No hay temas creados");
             return ;
         }
        for (int y = 0; y < asignatura.getNumTemas(); y++) {
            System.out.println(y++ + ". " + asignatura.getTemas()[y].toString());
        }

         int numTema = lecturaSC.leerEntero("Número de tema a mostrar", 0, asignatura.getTemas().length);

         System.out.println(asignatura.getTemas()[numTema - 1].toString());
     }

     public static void mostrarTemas(){
         if (asignaturas != null && asignaturas.length != 0) {
             System.out.println("No hay asignaturas creadas");
             return ;
         }

         for (int x = 0; x < asignaturas.length; x++) {
             System.out.println(x++ + ". " + asignaturas[x].getNombre());
         }

         int numAsignatura = lecturaSC.leerEntero("Número de asignatura a mostrar", 0, asignaturas.length);

         Asignatura asignatura = asignaturas[numAsignatura - 1];

         if (asignatura.getTemas() != null && asignatura.getTemas().length != 0) {
             System.out.println("No hay temas creados");
             return ;
         }
         for (int y = 0; y < asignatura.getNumTemas(); y++) {
             System.out.println(asignatura.getTemas()[y].toString());
         }
     }

     public static void mostrarAsignaturas(){
         if (asignaturas != null && asignaturas.length != 0) {
             System.out.println("No hay asignaturas creadas");
             return ;
         }

         for (int x = 0; x < asignaturas.length; x++) {
             System.out.println(asignaturas[x].toString());
         }
     }

     public static void asignarCalif(){
         if (asignaturas != null && asignaturas.length != 0) {
             System.out.println("No hay asignaturas creadas");
             return ;
         }

         for (int x = 0; x < asignaturas.length; x++) {
             System.out.println(x++ + ". " + asignaturas[x].getNombre());
         }

         int numAsignatura = lecturaSC.leerEntero("Número de asignatura a mostrar", 0, asignaturas.length);

         Asignatura asignatura = asignaturas[numAsignatura - 1];

         if (asignatura.getTemas() != null && asignatura.getTemas().length != 0) {
             System.out.println("No hay temas creados");
             return ;
         }
         for (int y = 0; y < asignatura.getNumTemas(); y++) {
             System.out.println(y++ + ". " + asignatura.getTemas()[y].toString());
         }

         int numTema = lecturaSC.leerEntero("Número de tema asignar calificación", 0, asignatura.getTemas().length);

         int calif = lecturaSC.leerEntero("Ingrese la calficación del tema", -1, 101);

         Tema tema = asignatura.getTemas()[numTema - 1];
         tema.setCalif(calif);
     }
}
