
 // ESTA CLASE REPRESENTA EL NUMERO DE TEMAS QUE PUEDE TENER UNA
 // ASIGNATURA DEL ITZ
 // POR: SALGADO SANTIAGO CHRISTOPHER POO
package MisClases;


public class Tema {

  //ATRIBUTOS, VARIABLES DE INSTANCIA.

 private String nombre;
 private String competencia;
 private int calif;

 //CONSTRUCTORES
    public Tema() {}

 public Tema(String nombre, String competencia){

     this.nombre=nombre;
     this.competencia=competencia;
     this.calif=0;
 }// FIN DEL CONSTRUCTOR 1

    public Tema(String nombre) {
        this(nombre,"");
    }// FIN DEL CONSTRUCTOR 2



  //METODOS SET Y GET.

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCompetencia() {
        return competencia;
    }

    public void setCompetencia(String competencia) {
        this.competencia = competencia;
    }

    public int getCalif() {
        return calif;
    }

    public void setCalif(int calif) {
        this.calif = calif;
    }



  //METODO TO STRING

    @Override
    public String toString() {
    return "Tema{" + "nombre=" + nombre + ", competencia=" + competencia + ", calif=" + calif + '}';
    }

}
