
package MisClases;

import MisUtilerias.lecturaSC;
import java.util.Scanner;


public class Asignatura {

    //ATRIBUTOS
  public static Scanner sc;
  private String nombre;
  private String clave;
  private int creditos;
  private int numTemas;
  private Tema[] temas;

  //CONSTRUCTORES

  public Asignatura(){

  }

  public Asignatura(String nombre, String clave, int creditos, int numTemas, Tema[] temas){
      this.nombre=nombre;
      this.clave=clave;
      this.creditos=creditos;
      this.numTemas=numTemas;
      this.temas = temas;
  }//FIN CONSTRUCTOR

 // METODOS SET Y GET
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public int getNumTemas() {
        return numTemas;
    }

    public void setNumTemas(int numTemas) {
        this.numTemas = numTemas;
        this.temas=new Tema[numTemas];//se cambia el tamaño del arreglo
    }

    public Tema[] getTemas() {
        return temas;
    }

    public void setTemas(Tema[] temas) {
        this.temas = temas;
        this.numTemas=temas.length;
    }

  // METODO String toString }

    @Override
    public String toString() {
        return "Asignaturas{" + "nombre=" + nombre + ", clave=" +
                clave + ", creditos=" + creditos + ", numTemas=" +
                numTemas + ", temas=" + temas + '}';
    }

    //OTROS METODOS

    public int calcularPromedio(){
        //variables de instancia
        int prom;
        int suma=0;
        for(int i=0; i<temas.length; i++){

          suma=suma+temas[i].getCalif();
        }
        prom=suma/temas.length;
        return prom;
    }

   public static void CrearAsignatura(Asignatura[]lista){
        int NumMateria;
        NumMateria = lecturaSC.leerEntero("¿Qué Asignatura quieres crear?",1,lista.length);
    }//Fin del CrearAsignación

}
