package MisClases;

import MisUtilerias.lecturaSC;

/**
 * 
 * @author Ricardo Serafin Ruvalcaba
 * 25 de Febrero del 2022
 */
public class Fecha {
 //variables de instancia
    private int dia;
    private int mes;
    private int year;
    
    //variable de clase
   public static   String [] MESES= {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
     "Agosto", "Septiembre", "Octubre","Noviembre", "Diciembre"};
    
    
    
    //Metodos Constructores

    public Fecha(int dia, int mes, int year) {
        this.dia = dia;
        this.mes = mes;
        this.year = year;
    } //constructor 1 

    public Fecha(int mes, int year) {
        //podemos invocar al constructor 1
        this(1, mes, year); // esta es la llamada a un constructor de esta clase
        //La instruccion de invocar a un constructor de la clase (this) debe ser la primera instruccion del metodo
       /* this.mes = mes;
        this.year = year;
        dia = 1; */
    } //Constructor 2
    
    public Fecha (int dia, String mes, int year){
    this.dia=dia;
    this.year=year;
    this.mes= this.getMes(mes); 
} //constructor 3
    
   public Fecha (String mes, int year){
       this(1,mes,year);  //Invoca al Constructor 3
   } // Constructor 4
    
    
    
    
    
    //Metodos set. Se utilizan para cambiar el valor de los atributos

    public void setDia(int dia) {
        this.dia = dia;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

     public void setMes(String mes) {
        this.mes = getMes(mes);
    }
    
    public void setYear(int year) {
        this.year = year;
    }
    
    
  //metodos get. Devuelven  el valor que tiene cada atributo

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    } //fin de getMes, versión 1
    
     public int getMes(String mes) {
         int m=0; //variable de retorno
       switch (mes){ //para determinar el numero del mes
           
      case "Enero": m= 1 ; break;
       case "Febrero": m=2; break;
       case "Marzo":  m=3; break;
       case "Abril": m=4; break;
       case "Mayo": m=5; break;
       case "Junio": m=6; break;
       case "Julio": m=7; break;
       case "Agosto": m=8; break;
       case "Septiembre": m=9; break;
       case "Octubre": m=10; break;
       case "Noviembre": m=11; break;
       case "Diciembre": ; m=12; break;     
      }   //fin de switch
        return m;
    } //fin de getMes version 2
    

   public String getMes(int nmes) {
         String nombreMes= this.MESES[nmes-1];
         return nombreMes;
    } //fin de getMes version 3
   
       
    public int getYear() {
        return year;
    }
    
    //metodos toString. Devuelve una representación en texto del objeto
    
    @Override
    public String toString(){ //genera la fecha en formato dd-mm mm- aa
        String texto;
        texto= (dia<10?("0" + dia): dia) + "-"+
        (mes<10?("0" + mes): mes) + "-" +
                year;  //25-02-2022
        return texto;   
    } //fin de toString
    
    
   //Otros métodos
    
    public String getFechaTexto (){ //el formato de la fecha es dd - nombre - aaaa
        
        String texto;
        texto= (dia<10?("0" + dia): dia) + "-" + this.getMes(mes) + "-" + year;
        return texto;
    }
    
    
    
    public Fecha sumaDias (int nd){
        //variable de retorno
        Fecha fechaNueva = this; // this hace referencia a todo el objeto que hace la llamada
        int sumaDias, sumaMeses, numDias=0;
        
        for (int d=1; d<=nd; d++)
            fechaNueva = fechaNueva.DiaSiguien(); //Funciona como un acumulador
        return fechaNueva; 
    } //fin de suma Dias 
    
    
    
    
    public Fecha DiaSiguien(){
        Fecha siguiente= null;
        int numDias= this.getDiasMes();
       
        int  nd=this.dia+1,  nm=this.mes, na=this.year;
        if (nd  > numDias){
            
            nd = 1;
            nm= this.mes + 1;
            if (nm > 12 ){
                nm=1;
                na=this.year + 1;
            }
        }
        siguiente= new Fecha (nd,nm,na);
        return siguiente;
    }//fin de Fecha diaSiguien
    
    
     public Fecha sumaMeses (int nm){
        //variable de retorno
        Fecha fechaNueva = null; 
        int mes= this.mes + nm;
        int na= this.year;
        if (mes> 12){
            
         na= this.year+(mes/12);
         mes= mes%12;
         
        }
        fechaNueva= new Fecha (this.dia, mes, na);
        return fechaNueva;     
    }
    
     
     public Fecha sumaYears (int na){
        //variable de retorno
        Fecha fechaNueva = null;  
        fechaNueva=  new Fecha(this.dia,this.mes,this.year +na);
        return fechaNueva;   
    }
       
     
   public int getDiasMes (){ 
       int numdias=0;
     switch (this.mes){
          
          case 1:
          case 3: 
          case 5:
          case 7: 
          case 8: 
          case 10: 
          case 12:
          numdias=31;
        break;
        
        case 4: 
       case 6: 
       case 9: 
       case 11: numdias= 30;
       break;
          
      case 2:
           if (this.year %4==0)
               numdias=29;
           else
            numdias=28;
           break;                 
      }   
       return numdias;
       
   } 
} // FIN DE LA CLASE (NO BORRAR)