
//DISEÑA E IMPLEMENTA UNA CLASE EN JAVA LLAMADA Nombre QUE CONTENGA LOS
//SIGUIENTES ATRIBUTOS: Nombre String, ApellidoPaterno String
//ApellidoMaterno String.

//SALGADO SANTIAGO CHRISTOPHER XB

package MisUtilerias;

import java.util.Scanner;


public class Nombre {
  
//ATRIBUTOS DE LA CLASE NOMBRE    
public static Scanner sc;
private String nombre;
private String apellidoPaterno;
private String apellidoMaterno;

//CONSTRUCTORES

    public Nombre(){
        
    }
    

//SET Y GET

    public void setNombre(String nombre) {
        this.nombre = nombre;
    
    }
     public String getNombre() {
        return nombre;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }
     public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

      public String getApellidoMaterno() {
        return apellidoMaterno;
    }

//toString
      
   public String toString(){
       
       
   return "NOMBRE: "+nombre+ "\n"+"APELLIDO PATERNO: "+apellidoPaterno+"\n"+
           "APELLIDO MATERNO: "+apellidoMaterno;
    
}   
      
      
      
//OTROS METODOS      
public static String leerNombre(String nom){
    
  String nombre;  
    sc=new Scanner(System.in);
    System.out.println(nom);
    nombre=sc.nextLine();
    return nombre;
}
public static String leerApellidoPaterno(String apeP){
    
  String apellidoP;  
    sc=new Scanner(System.in);
    System.out.println(apeP);
    apellidoP=sc.nextLine();
    return apellidoP;
}

 public static String leerApellidoMaterno(String apeM){
    
  String apellidoM;  
    sc=new Scanner(System.in);
    System.out.println(apeM);
    apellidoM=sc.nextLine();
    return apellidoM;
}
    
}

