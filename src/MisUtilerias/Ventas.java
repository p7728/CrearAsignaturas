package MisUtilerias;
public class Ventas{
    //atributos
    private int dia;
    private int mes;
    private int year;
    private int cantidad;
    
    //variables de clase
    public static String[] lista = {"Jabón   $12\n", "Leche   $25\n", "Agua   $10\n", "Azúcar   $22\n",
    "Galletas   $8"};

    public Ventas(int dia, int mes, int year) {
        this.dia = dia;
        this.mes = mes;
        this.year = year;
    }

    public Ventas(int cantidad) {
        this.cantidad = cantidad;
    }
//métodos set
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }
         
    public void setDia(int dia) {
        this.dia = dia;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public void setMes(String mes){
        this.mes = 1;
    }
    public void setYear(int year) {
        this.year = year;
    }
    
    //métodos get
    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }
    
    public int getYear() {
        return year;
    }
    
    //toString           //ofrece una versión en cadena de la fecha dd-mm-aa
    //toString es un método heredado de Object y que vamos a redefinir
    @Override
    public String toString(){ //formato de la fecha toString es dd-mm-aaaa
        //variable de retorno
        String cadena = "";
        cadena = (dia<10?"0"+dia:dia)+ "-"+
                (mes<10?"0"+mes:mes)+ "-" + year;
        return cadena;
    }
     public static String defSw(int cJab, int cL, int cA, int cAz, int cG){
        String ventas = "";
        cJab = 0;
        cL = 0;
        cA = 0;
        cAz = 0;
        cG = 0;
        return ventas;
    }
    public static String carrito(int cJab, int totalJab, int cL, int totalL, int cA, int totalA, int cAz, int totalAz, int cG, int totalGa){
        String cadena =  "C A R R I T O  D E  C OM P R A S\n\n"
                            + "Jabón           cantidad "+cJab+"       total  "+totalJab
                    + "\nLeche           cantidad "+cL+"       total  "+totalL
                    + "\nAgua              cantidad "+cA+"       total  "+totalA
                    + "\nAzúcar            cantidad " +cAz+"       total  "+totalAz
                    + "\nGalletas         cantidad "+cG+"       total  "+totalGa;
        return cadena;
    }
    public static String fish (String fecha, int cJab, int totalJab, int cL, int totalL, int cA, int totalA, int cAz, int totalAz, int cG, int totalGa, int total){
        String cadena = "                    T I C K E T     D E    C O M P R A\n\n"
                            + "                    Fecha "+fecha
                            +"\n\nPRODUCTO            CANTIDAD            TOTAL"
                    + "\n Jabón                          "+cJab+"                             "+totalJab
                    +"\n Leche                          "+cL+"                             "+totalL
                    +"\n Agua                             "+cA+"                             "+totalA
                    +"\n Azúcar                         "+cAz+"                             "+totalAz
                    +"\n Galletas                       "+cAz+"                             "+totalGa
                    +"\n\n    T O T A L                                                  "+total;
        return cadena;
    }
    
} //fin
