package MisUtilerias;
import java.util.Scanner;

public class Persona {
    //ATRIBUTOS 
    private static Scanner SC;
    private String Nombre;
    private int Fecha;
    private String Curp;
 
    
    //METODO CONSTRUCTOR
  public Persona(){
             
}
  
  //METODOS SET Y GET
 
  public void SetNombre(String Nombre){
      this.Nombre = Nombre;
  }
      public String getNombre() {
        return Nombre;
    }
  public void SetFecha(int fecha){
      this.Fecha = Fecha;
  }
    public int getFecha(){
        return Fecha;
    }
  public void SetCurp(String Curp){
    this.Curp = Curp;
  }
    public String getCurp() {
        return Curp;
    }

    
 
    
  //Método String toString
public String toString(){
    return "LOS DATOS SON: "+ "\n"+"Nombre:" + Nombre + "\n" +"Curp: " + Curp + "\n" +
            "Fecha de nacimiento: ";
}
    
    //OTROS METODOS
        public static int leerInt(String mensaje){
        int Valor;
        Scanner sc = new Scanner(System.in);
        System.out.println(mensaje);
        Valor = sc.nextInt();
        return Valor;
    }//Fin de LeerEntero
    
    public static int leerInt(String mensaje, int menor, int mayor){
        //Derfinir la variable de retorno
        int Valor;
        SC = new Scanner(System.in);
        do{
             System.out.println(mensaje);
              Valor = SC.nextInt();
              if (Valor< menor || Valor> mayor)
              System.out.println("Valor fuera de rango /n" + 
                      "Debe estar en un rango entre " + menor + "y" + mayor);     
        }while(Valor < menor || Valor > mayor );
        return Valor;
    }//Fin del leerInt

     public  static String leerString (String msg){
        String Valor = null; 
        //Crear el objeto de Scanner de mi variable de clase
        SC= new Scanner(System.in);
         System.out.println(msg);
         Valor = SC.nextLine();
         return Valor;
    }//Fin del String
     
}//Fin de la clase


