
package MisUtilerias;
import java.util.Scanner;
public class lecturaSC
{
    //ATRIBUTOS
    public static Scanner sc;
    //CONSTRUCTORES
    //no los nececsito en este momento
    //java crea el defaut lecturaSC

    //SET Y GET
    //no hay avriables privadas o protegidas
    //no hay nececsidad de estos metodos

    //toString
    //no hay nececsidad de ver el objeto en pantalla
    //solo queremos usarlo

    //otros metodos
    public static int leerEntero(String msg){
        // definir la variable del retorno
     int valor=0;
     sc= new Scanner(System.in);
        System.out.println(msg);
        valor= sc.nextInt();
        return valor;
    }

    public static int leerEntero(String msg, int may) {
        //definir la variable de retorno
        int valor=0;
        sc=new Scanner(System.in);

        do {
            System.out.println(msg);
            valor=sc.nextInt();
            if (valor < may)
                System.out.println("valor debe ser mayor a " + may);
        } while (valor  < may);

        return valor;
    }

    public static int leerEntero(String msg, int men, int may) {
         //definir la variable de retorno
         int valor=0;
         sc=new Scanner(System.in);

         do {
             System.out.println(msg);
             valor=sc.nextInt();
             if (valor < men || valor > may)
                 System.out.println("valor fuera de rango debe de ser "
                + men + " a " +may );
         } while (valor < men ||valor  > may);

         return valor;
    }

 public static double leerReal(String msg){
        // definir la variable del retorno
     double valor=0;
     sc= new Scanner(System.in);
        System.out.println(msg);
        valor= sc.nextDouble();
        return valor;
    }
    public static double leerReal(String msg, double men, double may){
     //definir la variable de retorno
     double valor=0;
     sc=new Scanner(System.in);
     do {
         System.out.println(msg);
         valor=sc.nextDouble();
         if (valor < men || valor > may)
             System.out.println("valor fuera de rango debe de ser "
            + men + " a " +may );
     }while (valor < men ||valor  > may);
     return valor;

}
    //leer un caracter
 public static char leerChar (String msg){

   char letra= '\0';
    sc=new Scanner(System.in);
     System.out.println(msg);
     letra=sc.next().charAt(0);
   return letra;




 }

 public static char leerChar(String msg, char [] validos){

    char letra='\0';
      sc=new Scanner(System.in);
     System.out.println(msg);
     letra=sc.next().charAt(0);
    return letra;
 }

public static String leerNombre(String nom){

  String nombre;
    sc=new Scanner(System.in);
    System.out.println(nom);
    nombre=sc.nextLine();
    return nombre;
}
public static String leerApellidoPaterno(String apeP){

  String apellidoP;
    sc=new Scanner(System.in);
    System.out.println(apeP);
    apellidoP=sc.nextLine();
    return apellidoP;
}

 public static String leerApellidoMaterno(String apeM){

  String apellidoM;
    sc=new Scanner(System.in);
    System.out.println(apeM);
    apellidoM=sc.nextLine();
    return apellidoM;
}




  public static boolean buscarEn (char car, char [] validos){
    boolean resultado= false;
    int celda=0;
    do{
        if (car == validos[celda])
            resultado= true;
        else
            celda++;
    }while (resultado== false && celda< validos.length);
      return resultado;
  } //fin de buscarEn


  public static String leerString (String msg){
      String valor= null;
        sc = new Scanner (System.in);
   System.out.println(msg);
   valor= sc.nextLine();
     return valor;
  }//fin de leer String


  public static String leerOpcion (String msg, String[] opciones){
     String opcion= null;
     String cadena= "OPCIONES DISPONIBLES \n";
     for (int c=0; c< opciones.length; c++ ){
         cadena= cadena + (c+1) + ". " + opciones[c] + "\n";
     }//fin del for
     cadena= cadena + msg + "\n";
     int num= leerEntero(cadena, 1,opciones.length);
    opcion= opciones[num-1];
     return opcion;
  }//fin del leerOpcion
}



