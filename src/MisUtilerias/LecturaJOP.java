// ESTA CLASE CONTIENE METODOS DIVERSOS UTILIZANDO CLASE JOPTIONPANE 
package MisUtilerias;

import javax.swing.JOptionPane;

/**
 *
 * @author Santiago
 */
public class LecturaJOP {
    
   public static int leerEntero(String msg){
        // definir la variable del retorno
     int valor=0;   
   String temp=JOptionPane.showInputDialog(msg);
   valor= Integer.parseInt(temp);
        return valor;
    }
    public static int leerEntero(String msg, int men, int may){
     //definir la variable de retorno
     int valor=0;   
     do {
   String temp=JOptionPane.showInputDialog(msg);
   valor= Integer.parseInt(temp);
         if (valor < men || valor > may)
             JOptionPane.showMessageDialog(null, "valor fuera de rango debe de ser "
            + men + " a " +may );
     }while (valor < men ||valor  > may);
     return valor;

}
    
 public static double leerReal(String msg){
        // definir la variable del retorno
     double valor=0;    
      String temp=JOptionPane.showInputDialog(msg);
        valor= Double.parseDouble(temp);
        return valor;
    }
    public static double leerReal(String msg, double men, double may){
     //definir la variable de retorno
     double valor=0;
  
     do {
         String temp=JOptionPane.showInputDialog(msg);
         valor=Double.parseDouble(temp);
         if (valor < men || valor > may)
            JOptionPane.showMessageDialog(null, "valor fuera de rango debe de ser "
            + men + " a " +may );
     }while (valor < men ||valor  > may);
     return valor;

}     
    //lectura de caracter 
 public static char leerChar (String msg){
  
   char letra= '\0'; 
    
     String temp=JOptionPane.showInputDialog(msg);
     letra=temp.charAt(0);
   return letra;       
     
 }      
 
  
public static char leerChar (String msg, char[] validos){
    char valor= '\0';
    boolean encontrado= false;
    do{
        
        String temp= JOptionPane.showInputDialog(msg);
   valor= temp.charAt(0);
   encontrado= buscarEn(valor,validos);
   if (encontrado== false);
   JOptionPane.showMessageDialog(null,"Caracter no valido");
    }while(encontrado==false);
    return valor;
}  //fin de leerchar
  
  public static boolean buscarEn (char car, char [] validos){
    boolean resultado= false;
    int celda=0;
    do{
        if (car == validos[celda])
            resultado= true;
        else
            celda++;
    }while (resultado== false && celda< validos.length);     
      return resultado;
  } //fin de buscarEn
  
  
  public static String leerString (String msg){
      String valor= null;
       valor= JOptionPane.showInputDialog(msg);
     return valor;         
  }//fin de leer String
  
    
    
 public static String leerOpcion (String msg, String[] opciones){
     String opcion= null;
     opcion= (String) JOptionPane.showInputDialog(
     null, // este es un objeto de tipo contenedor, al no tenerlo se envia el null
     msg, //es el mensaje para el usuario
     "OPCIONES DISPONIBLES", //Titulo de la ventana
     JOptionPane.QUESTION_MESSAGE, // el tipo de mensaje o de entrada
     null, // objeto de tipo Icon, al no tenerlo se envia null
     opciones, // lista de opciones
     opciones[0]  //opcion que aparecera como default
     ); 
     return opcion;
 }   // fin de leerOpcion  
}
